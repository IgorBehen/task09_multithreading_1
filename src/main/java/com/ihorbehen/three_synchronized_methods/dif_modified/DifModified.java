package com.ihorbehen.three_synchronized_methods.dif_modified;

import java.time.LocalDateTime;

public class DifModified {
        private static final Object lock = new Object();
//        private static final Object lock1 = new Object();
//        private static final Object lock2 = new Object();
        private static int task = 0;

        static class SynchMeth1 implements Runnable {
            int counter;
            int n = 1;

            @Override
            public void run() {
                while (counter < 20) {
                    synchronized (lock) {
                        counter++;
                        String trdName = Thread.currentThread().getName();
                        try {
                            Thread.sleep(500);
                            System.out.println("class SynchMeth1: Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                                    + LocalDateTime.now());
                            task += 1;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }

    static class SynchMeth2 implements Runnable {
        int counter;
        int n = 1;

        @Override
        public void run() {
            while (counter < 20) {
                synchronized (/*lock1*/lock) {
                    counter++;
                    String trdName = Thread.currentThread().getName();
                    try {
                        Thread.sleep(500);
                        System.out.println("class SynchMeth2: Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                                + LocalDateTime.now());
                        task += 1;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    static class SynchMeth3 implements Runnable {
        int counter;
        int n = 1;

        @Override
        public void run() {
            while (counter < 20) {
                synchronized (/*lock2 */lock) {
                    counter++;
                    String trdName = Thread.currentThread().getName();
                    try {
                        Thread.sleep(500);
                        System.out.println("class SynchMeth3: Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                                + LocalDateTime.now());
                        task += 1;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

        public static void main(String[] args) {
            Thread t1 = new Thread(new DifModified.SynchMeth1());
            Thread t2 = new Thread(new DifModified.SynchMeth2());
            Thread t3 = new Thread(new DifModified.SynchMeth3());
            t1.start();
            t2.start();
            t3.start();
            try {
                t1.join();
                t2.join();
                t3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task = " + task);
        }
}
