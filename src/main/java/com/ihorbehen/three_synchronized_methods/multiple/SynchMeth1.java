package com.ihorbehen.three_synchronized_methods.multiple;

import java.time.LocalDateTime;

public class SynchMeth1 implements Runnable {
    private int counter;
    private int n = 1;

    @Override
    public void run() {
        while (counter < 200) {
            synchronized (ThreeSynchronizedMethodsModified.lock) {
                counter++;
                String trdName = Thread.currentThread().getName();
                try {
                    Thread.sleep(500);
                    System.out.println("Thread 1 name : " + trdName + " ### " + n++ + " times" + " --> "
                            + LocalDateTime.now());
                    ThreeSynchronizedMethodsModified.task += 1;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new SynchMeth1());
        Thread t2 = new Thread(new SynchMeth1());
        Thread t3 = new Thread(new SynchMeth1());
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("task 1 = " + ThreeSynchronizedMethodsModified.task);
    }
}